# README #

This script was created to create list of the TOP 500 sites published on http://www.alexa.com/topsites. It can either
generate a list for the global TOP 500 or for a specific country.

## License ##

This script is licensed under the GNU General Public License in version 3. See http://www.gnu.org/licenses/ for further details.

## Usage ##
Create a list of the global TOP 500.
```
$ ./alexa_crawler.py 
[+] requesting site: http://www.alexa.com/topsites
[+] requesting site: http://www.alexa.com/topsites/global;1
...
```

Create a list of the german TOP 500 instead of the global TOP 500.
```
$ ./alexa_crawler.py --de
[+] requesting site: http://www.alexa.com/topsites/countries/DE
[+] requesting site: http://www.alexa.com/topsites/countries;1/DE
[+] requesting site: http://www.alexa.com/topsites/countries;2/DE
...
```

It is also possible to create a list for an other country by using the option **--other**.
To create an list for the france TOP 500 you can use the following example.
```
$ ./alexa_crawler.py --other fr
[+] requesting site: http://www.alexa.com/topsites/countries/FR
[+] requesting site: http://www.alexa.com/topsites/countries;1/FR
[+] requesting site: http://www.alexa.com/topsites/countries;2/FR
[+] requesting site: http://www.alexa.com/topsites/countries;3/FR
[+] requesting site: http://www.alexa.com/topsites/countries;4/FR

...
```