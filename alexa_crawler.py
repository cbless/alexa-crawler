#!/usr/bin/env python

# Copyright (C) 2016 Christoph Bless
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Description:
# This script was created to create list of the TOP 500 sites published on http://www.alexa.com/topsites. It can either
# generate a list for the global TOP 500 or for a specific country.
#
#
#
# This script is available on bitbucket.org see:
#     https://bitbucket.org/cbless/alexa-crawler

from bs4 import BeautifulSoup
import urllib2
import time


def generate_global():
    """
    Generates a list of URLs needed to crawl the global TOP 500.

    :return: list of URLs
    """
    sites = ["http://www.alexa.com/topsites"]
    sites.extend(["http://www.alexa.com/topsites/global;{0}".format(i) for i in xrange(1, 20)])
    return sites


def generate_sites(country):
    """
    Generates a list of URLs needed to crawl the TOP 500 for the given country. The given country code must be in format
    [a-zA-Z]{2}.

    :param country: Country code for the list to be created
    :return: list of URLs
    """
    sites = ["http://www.alexa.com/topsites/countries/{0}".format(country.upper())]
    sites.extend(["http://www.alexa.com/topsites/countries;{0}/{1}".format(i, country.upper()) for i in xrange(1, 20)])
    return sites


def find_sites(html):
    """
    Function that extracts the URLs from the given site.

    :param html: HTML code of the site to parse
    :return: list of URLs found in the html code
    """
    bs = BeautifulSoup(html, "lxml")
    sites = []
    paragraphs = bs.find_all('p', {'class': 'desc-paragraph'})
    for p in paragraphs:
        sites.append(p.a.text)
    return sites


def crawl(urls=[], user_agent="Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1", sleep=0):
    """
    Crawler which visits the list of given URLs and extracts the TOP sites by using the function find_sites(html). The
    parameter urls must contains a list of URLs to visit. The generate this list the functions generate_global and
    generate_sites can be used. The parameter user_agent can be used the change the HTTP User-Agent string.

    :param urls: Lists of URLs to visit
    :param user_agent: HTTP User-Agent that should be used
    :return: list of URLs for the TOP sites found in the html code of the visited sites
    """
    results = []
    for url in urls:
        print "[+] requesting site: {0}".format(url)
        try:
            req = urllib2.Request(url)
            # change the HTTP User-Agent header
            req.add_header('user-agent', user_agent)
            response = urllib2.urlopen(req)

            if response.code == 200:
                html = response.read()
                results.extend(find_sites(html)) # extract the TOP sites
        except Exception as e:
            print "[-] {0}".format(e)
            pass
        time.sleep(sleep)
    return results


def main(urls=[], sleep=0, file=None):
    """
    Runs the crawler and prints the found TOP sites to stdout. If the parameter file is set this TOP sites will be
    written to the specified file.

    :param urls: list of URLs to crawl
    :param sleep: The to sleep between each Request
    :param file: Filename to store the results
    """
    topsites = crawl(urls=urls, sleep=sleep)

    if args.file:
        with open(args.file, "a") as f:
            f.write("\n".join(topsites))

    for i in topsites:
        print i


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="I am a simple crawler for alexa.com's TOP 500 sites")
    parser.add_argument("-f", "--file", metavar="FILE", type=str, required=False, default=None,
                        help="File to store the top 500 sites from alexa.com")
    parser.add_argument("--de", action="store_true", help="Download German TOP 500 instead of the global TOP 500")
    parser.add_argument("--us", action="store_true", help="Download US TOP 500 instead of the global TOP 500")
    parser.add_argument("--jp", action="store_true", help="Download Japan TOP 500 instead of the global TOP 500")
    parser.add_argument("--other", metavar='LANG', default=None, help="Language code")
    parser.add_argument("--sleep", type=int, required=False, default=0)
    args = parser.parse_args()

    # generate the list of URLs to crawl
    if args.us is True:
        urls = generate_sites("US")
    elif args.de is True:
        urls = generate_sites("DE")
    elif args.jp is True:
        urls = generate_sites("JP")
    elif args.other is not None:
        urls = generate_sites(args.other)
    else:
        urls = generate_global()

    try:
        main(urls, args.sleep, args.file)
    except KeyboardInterrupt:
        import sys
        sys.exit(0)
